package fimoapp.mynfcreader;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class GetTagHttpAsync extends AsyncTask<String, Void, Boolean> {

	private Activity calledActivity = null;
	//private static final String tagURL = "http://192.168.1.33/tag.xml";
	private static final String tagURL = "http://flamingo.luna.ddns.vc/tag.xml";
	
	public GetTagHttpAsync(Activity activity)
	{
		this.calledActivity = activity;
	}
	
	@Override
	protected Boolean doInBackground(String... urls) {
		
		// xmlをとる
		String xml = getXml();
		if (xml.isEmpty())
		{
			cancel(true);
			return false;
		}
		
		// idm抽出
		String idm = getIdm(xml);
		if (idm.isEmpty())
		{
			cancel(true);
			return false;
		}
		
		if (setIdmDb(idm) == false)
		{
			cancel(true);
			return false;
		}
		
		return true;
	}
	
	/** xmlをget */
	private String getXml()
	{
		try{
			 HttpGet method = new HttpGet( tagURL );
			 
			 DefaultHttpClient client = new DefaultHttpClient();
			
			 //クッキーを設定
			 //client.setCookieStore(cookieStore);
			
			 // ヘッダを設定する
			 method.setHeader( "Connection", "Keep-Alive" );
			
			 HttpResponse response = client.execute( method );
			 int status = response.getStatusLine().getStatusCode();
			 if ( status != HttpStatus.SC_OK )
			 {
			     throw new Exception( "" );
			 }
			
			 //GET結果を取得
			 String xml = EntityUtils.toString( response.getEntity(), "UTF-8" ); 
			    
			 return xml;
		}
		catch (UnsupportedEncodingException e)
		{
			return "";
		}
		catch (ClientProtocolException e)
		{
			Log.e("!!!", "ClientProtocolException", e);
			return "";
		}
		catch (IOException e)
		{
			Log.e("!!!", "IOException", e);
			return "";
		}
		catch ( Exception e )
		{
			Log.e("!!!", "??Exception", e);
		    return "";
		}		
	}
	
	/** xmlからidmを抽出 */
	private String getIdm(String xml)
	{
		// とりあえずはテキスト解析しておく
		int start = xml.indexOf("<idm>") + 5;
		if (start == -1) return "";
		
		int end = xml.indexOf("</idm>", start);
		if (end == -1) return "";
		
		String idm = xml.substring(start, end);
		return idm;
	}
	
	/** IDMをDBに設定 */
	private boolean setIdmDb(String idm)
	{
		if (calledActivity == null) return false;
		
		SaveSetting setting = new SaveSetting(calledActivity);
		setting.setIdm(idm);
		return true;
	}

	@Override
    protected void onPostExecute(Boolean result) {
		if (result == true)
		{
			Toast.makeText(this.calledActivity, "タグの取得に成功しました。", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(this.calledActivity, "タグの取得に失敗しました...", Toast.LENGTH_LONG).show();
		}
	}
}
