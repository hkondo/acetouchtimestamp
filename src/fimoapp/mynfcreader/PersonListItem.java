package fimoapp.mynfcreader;

import java.io.Serializable;

public class PersonListItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name_ = "";
	private String userid_ = "";
	private String password_ = "";
	private String laststamp_ = "";
	private String idm_ = "";
	
	public PersonListItem(String idm, String name)
	{
		idm_ = idm;
		name_ = name;
	}
	
	public PersonListItem(String idm, String name, String userid, String password, String laststamp)
	{
		idm_ = idm;
		name_ = name;
		userid_ = userid;
		password_ = password;
		laststamp_ = laststamp;
	}
	
	/** getter&setter */
	public String getName() { return name_; }
	public void setName(String name) { name_ = name; }
	public String getIdm() { return idm_; }
	public void setIdm(String idm) { idm_ = idm; }
	public String getUserid() { return userid_; }
	public String getPassword() { return password_; }
	public String getLaststamp() { return laststamp_; }
}
