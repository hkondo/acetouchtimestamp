package fimoapp.mynfcreader;

public class SaveSettingData {

	private String idm_ = new String("");
	private String userid_ = new String("");
	private String password_ = new String("");
	private String laststamp_ = new String("");
	
	public SaveSettingData()
	{
	}
	
	public String getIdm()
	{
		return idm_;
	}
	
	public String getUserid()
	{
		return userid_;
	}
	
	public String getPassword()
	{
		return password_;
	}
	
	public String getLaststamp()
	{
		return laststamp_;
	}
	
	public void setIdm(String idm)
	{
		idm_ = idm;
	}
	
	public void setUserid(String userid)
	{
		userid_ = userid;
	}
	
	public void setPassword(String password)
	{
		password_ = password;
	}
	
	public void setLaststamp(String laststamp)
	{
		laststamp_ = laststamp;
	}
	
	public void setData(String idm, String userid, String password, String laststamp)
	{
		idm_ = idm;
		userid_ = userid;
		password_ = password;
		laststamp_ = laststamp;
	}
}
