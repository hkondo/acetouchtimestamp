package fimoapp.mynfcreader;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class LoginFormDialg extends DialogFragment {

	public LoginFormDialg()
	{
		super();
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View content = inflater.inflate(R.layout.loginform, null, false);
        
        builder.setView(content);
        builder.setMessage("ユーザー情報");
        
        final EditText editUserid = (EditText) content.findViewById(R.id.login_form_user);
        final EditText editPassword = (EditText) content.findViewById(R.id.login_form_password);
        
        SaveSetting setting = new SaveSetting(getActivity());
        editUserid.setText(setting.getUserid());
        editPassword.setText(setting.getPassword());
        
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            
			@Override
			public void onClick(DialogInterface dialog, int which) {
	    		
				SaveSetting setting = new SaveSetting(getActivity());
				setting.setUserInfo(editUserid.getText().toString(), editPassword.getText().toString());
			}
        });
        builder.setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		
		return builder.create();
	}
}
