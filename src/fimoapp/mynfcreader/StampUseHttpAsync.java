package fimoapp.mynfcreader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;


public class StampUseHttpAsync extends AsyncTask<String, Void, Boolean> {

	private Activity calledActivity = null;
	private static final int id_loginURL = 0;
	private static final int id_getURL = 1;
	private static final int id_userid = 2;
	private static final int id_password = 3;
	
	private CallBackTask callbacktask;
	
	public StampUseHttpAsync(Activity activity)
	{
		this.calledActivity = activity;
	}
	
	@Override
	protected Boolean doInBackground(String... urls) {
		// いまいちだが[0]がlogin用URLで、[1]がgetするURLとする
		// [2]がユーザーIDで[3]がパスワード。。だっせぇ
		if (urls.length != 4)
		{
			cancel(true);
			return false;
		}
		CookieStore cookie = login(urls[id_loginURL], urls[id_userid], urls[id_password]);
		if (cookie == null)
		{
			cancel(true);
			return false;
		}
		if (getHtmlUseCookie(cookie, urls[id_getURL]) == false)
		{
			cancel(true);
			return false;
		}
		return true;
	}
	// ログインして成功したらクッキーストアを返す
	private CookieStore login(String url, String userid, String password)
	{
		CookieStore cookieStore;
		try{
			HttpClient client = new DefaultHttpClient();
	    	HttpPost httppost = new HttpPost(url);
	
	    	//POST送信するデータを格納
	    	List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
	    	nameValuePair.add(new BasicNameValuePair("username", userid));
	    	nameValuePair.add(new BasicNameValuePair("password", password));
	    	
	    	//POST送信
	    	httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));
	    	HttpResponse response = client.execute(httppost);
	    	
	        int status = response.getStatusLine().getStatusCode();
	        if ( status != HttpStatus.SC_OK )
	        {
	            throw new Exception( "" );
	        }
	        //クッキーを保持(GET時に使う)
	        cookieStore = ((AbstractHttpClient) client).getCookieStore();
	         
	        //クッキー内容の取得デモ(単にクッキーの中身をデバグ表示するだけ)
	        {
	            //List<Cookie> clist = cookieStore.getCookies();
	
	            //Cookie cookie = clist.get(0);
	            //System.out.println("クッキー name value " + cookie.getName() + " " + cookie.getValue());
	        }
	        return cookieStore;
	    }
	    catch (UnsupportedEncodingException e)
	    {
	    	return null;
	    }
	    catch (ClientProtocolException e)
	    {
	    	Log.e("!!!", "ClientProtocolException", e);
	    	return null;
	    }
	    catch ( Exception e )
	    {
	    	Log.e("!!!", "??Exception", e);
	        return null;
	    }
	}
	
	// クッキーを使ってGetする
	private boolean getHtmlUseCookie(CookieStore cookieStore, String url)
	{
		try{
		 HttpGet method = new HttpGet( url );
		 
		 DefaultHttpClient client = new DefaultHttpClient();
		
		 //クッキーを設定
		 client.setCookieStore(cookieStore);
		
		 // ヘッダを設定する
		 method.setHeader( "Connection", "Keep-Alive" );
		
		 HttpResponse response = client.execute( method );
		 int status = response.getStatusLine().getStatusCode();
		 if ( status != HttpStatus.SC_OK )
		 {
		     throw new Exception( "" );
		 }
		
		 //GET結果を取得
		 //String html = EntityUtils.toString( response.getEntity(), "UTF-8" ); 
		    
		    return true;
		}
		catch (UnsupportedEncodingException e)
		{
			return false;
		}
		catch (ClientProtocolException e)
		{
			Log.e("!!!", "ClientProtocolException", e);
			return false;
		}
		catch (IOException e)
		{
			Log.e("!!!", "IOException", e);
			return false;
		}
		catch ( Exception e )
		{
			Log.e("!!!", "??Exception", e);
		    return false;
		}
	}
	
	@Override
    protected void onPostExecute(Boolean result) {
		if (result == true)
		{
			Toast.makeText(this.calledActivity, "処理完了！", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(this.calledActivity, "処理失敗...", Toast.LENGTH_LONG).show();
		}
		// 通知
		callbacktask.callBack(result);
        callbacktask = null;
	}
	
	public void setOnCallBack(CallBackTask _cbj) {
	    callbacktask = _cbj;
	}
	
	public static class CallBackTask {
        public void callBack(boolean result) {
        }

        public void callProgress(Integer progress) {
        	// ここでなにかするならスレッドが必要だよ
        }
    }
}
