package fimoapp.mynfcreader;

import java.util.Calendar;

import android.os.Bundle;
import android.provider.Settings;
import android.app.Activity;
import android.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.graphics.Typeface;
import android.widget.TextView;
import android.widget.Toast;
import android.text.format.DateFormat;
import android.util.Log;

/**
 * メインのアクティビティ
 * @author hitomi
 *
 */
public class MainActivity extends Activity {
	
	private static final String TAG = "MainActivity";
	private static final String URL_LOGIN = "http://www.ace-software.com:8888/login?";
	private static final String URL_ENTRY = "http://www.ace-software.com:8888/entry";
	private static final String URL_LEAVE = "http://www.ace-software.com:8888/leave";

	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
    ConnectivityManager connectivityManager;
    	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
        String action = intent.getAction();
		setContentView(R.layout.activity_main);
		
		// フォント読み込み
		Typeface typeface = FontUtility.getTypefaceFromAssets( this, "fontawesome-webfont.ttf" );
		
		// 画面随時更新系しょきか
		initDisplayItem();
		
		// 設定ボタン
		{
			// コントロールの表示フォント指定
			TextView textView = (TextView)this.findViewById(R.id.button_setting);
			textView.setTypeface(typeface);
			// アイコンとして表示する文字の生成
			char   unicode  = 0xF013;
			String iconText = String.valueOf( unicode );
			// アイコン表示
			textView.setText( iconText );
			// クリックイベント
			textView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(MainActivity.this, SettingListActivity.class);
					startActivity(intent);
				}
			});
		}
		
		// nfc設定開く
		{
			TextView textView = (TextView)this.findViewById(R.id.button_nfc);
			textView.setTypeface(typeface);
			// アイコンとして表示する文字の生成
			char   unicode  = 0xF085;
			String iconText = String.valueOf( unicode );
			// アイコン表示
			textView.setText( iconText );
			// クリックイベント
			textView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS));
				}
			});
		}
		
		// ためしに起動
		//Intent intent2 = new Intent(MainActivity.this, LoginFormActivity.class);
		//startActivity(intent2);
		
		//☆☆☆暫定でここでDBセットアップ
		//SaveSetting setting = new SaveSetting(MainActivity.this);
		//setting.addPerson("baf422", "こんどう", "kondo", "kondo0");
	
	    /* ConnectivityManagerの取得 */
		connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		
		// NFCかどうかActionの判定
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)
        ||  NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
        ||  NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {
        	Log.d(TAG, "NFC DISCOVERD:" + action);
            // IDmを表示させる
            String idm = getIdm(getIntent());
            if (idm != null) {
            	//TextView idmView = (TextView) findViewById(R.id.idm);
            	//idmView.setText(idm);
            	stamp(idm);
            }
        }
		mNfcAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
		mPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
				new Intent(getApplicationContext(), getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
    @Override
	protected void onResume() {
		super.onResume();
		if (mNfcAdapter != null) {
			setNfcIntentFilter(this, mNfcAdapter, mPendingIntent);
		}
		initDisplayItem();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mNfcAdapter != null) {
			mNfcAdapter.disableForegroundDispatch(this);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
        String idm = getIdm(intent);
        if (idm != null) {
        	//TextView idmView = (TextView) findViewById(R.id.idm);
        	//idmView.setText(idm);
        	stamp(idm);
        }
	}
	
	/**
	 *  表示を初期化
	 *  何回コールしてもいいようにすること
	 */
	private void initDisplayItem()
	{
		// フォント読み込み
		Typeface typeface = FontUtility.getTypefaceFromAssets( this, "fontawesome-webfont.ttf" );
		
		// メインアイコン
		{
			// コントロールの表示フォント指定
			TextView textView = (TextView)this.findViewById(R.id.home_icon);
			textView.setTypeface(typeface);
			// アイコンとして表示する文字の生成
			char   unicode = 0;
			if (isRegisteredTag() == false)
			{
				// タグ無し
				unicode  = 0xF06A;
			}
			else if (isStampedToday() == true)
			{
				// ビル
				unicode  = 0xF1AD;
			}
			else
			{
				// 家
				unicode  = 0xF015;
			}
			
			String iconText = String.valueOf( unicode );
			// アイコン表示
			textView.setText( iconText );
		}
	}

	/**
     * IDmを取得する
     * @param intent
     * @return
     */
	private String getIdm(Intent intent) {
		String idm = null;
		StringBuffer idmByte = new StringBuffer();
		byte[] rawIdm = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
		if (rawIdm != null) {
			for (int i = 0; i < rawIdm.length; i++) {
				idmByte.append(Integer.toHexString(rawIdm[i] & 0xff));
			}
			idm = idmByte.toString();
		}
		Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		String[] techList = tag.getTechList();
		if(techList != null)
		{
			for(int i =0; i < techList.length; i++)
			{
				Log.d("NFC",techList[i]);
			}
		}
		return idm;
	}
	
	/**
	 * フォアグラウンドディスパッチシステムで、アプリ起動時には優先的にNFCのインテントを取得するように設定する
	 */
	private void setNfcIntentFilter(Activity activity, NfcAdapter nfcAdapter, PendingIntent seder) {
		// NDEF type指定
		IntentFilter typeNdef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
		try {
			typeNdef.addDataType("*/*");
		} catch (MalformedMimeTypeException e) {
			e.printStackTrace();
		}
		// NDEF スキーマ(http)指定
		IntentFilter httpNdef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
		httpNdef.addDataScheme("http");
		IntentFilter[] filters = new IntentFilter[] {
				typeNdef, httpNdef
		};
		// TECH指定
		String[][] techLists = new String[][] {
				new String[] { IsoDep.class.getName() },
				new String[] { NfcA.class.getName() },
				new String[] { NfcB.class.getName() },
				new String[] { NfcF.class.getName() },
				new String[] { NfcV.class.getName() },
				new String[] { Ndef.class.getName() },
				new String[] { NdefFormatable.class.getName() },
				new String[] { MifareClassic.class.getName() },
				new String[] { MifareUltralight.class.getName() }
			};
		nfcAdapter.enableForegroundDispatch(activity, seder, filters, techLists);
	}
	
	// ネットワークの状態を見る
	private boolean checkCanConnectNetwork()
	{
		// ネットワークの接続を見る
	    NetworkInfo nInfo = connectivityManager.getActiveNetworkInfo();

	    if (nInfo == null) {
	        return false;
	    }

	    if (nInfo.isConnected()) {
	        /* NetWork接続可 */
	        return true;
	    } else {
	        /* NetWork接続不可 */
	        return false;
	    }
	}
	
	// 出退勤する
	private void stamp(String idm)
	{
		SaveSetting setting = new SaveSetting(MainActivity.this);
		String regIdm = setting.getIdm();
				
		// 登録がない
		if (regIdm.isEmpty())
		{
			Toast.makeText(this, "タグが登録されていません。", Toast.LENGTH_LONG).show();
			return;
		}
		
		// 登録されているIDmか？
		if (idm.compareTo(regIdm) != 0)
		{
			Toast.makeText(this, "登録されているタグと異なります。", Toast.LENGTH_LONG).show();
			return;
		}
		
		// ネットワーク接続チェック
		if (!checkCanConnectNetwork())
		{
			Toast.makeText(this, "ネットワークに接続できません。", Toast.LENGTH_LONG).show();
			return;
		}
		
		// ログイン設定チェック
		if (setting.getUserid().isEmpty() || setting.getPassword().isEmpty())
		{
			Toast.makeText(this, "ユーザー情報が設定されていません。", Toast.LENGTH_LONG).show();
			return;
		}
		
		String laststamp = setting.getLastStamp();
		
		// 現在日付
		String nowdate = (String) DateFormat.format("yyyy/MM/dd", Calendar.getInstance());
		Log.d("NOW", nowdate);
		if (nowdate.compareTo(laststamp) == 0)
		{
			// 退勤
        	
        	endOfWork(setting.getUserid(), setting.getPassword());
		}
		else
		{
			// 出勤
			
			// DB更新
			setting.setLastStamp(nowdate);
			
			startOfWork(setting.getUserid(), setting.getPassword());
		}
		initDisplayItem();
	}
	
	// 出勤
	private boolean startOfWork(String userid, String password)
	{
		String urls[] = {URL_LOGIN, URL_ENTRY, userid, password};
		StampUseHttpAsync http = new StampUseHttpAsync(this);
		http.setOnCallBack(new StampUseHttpAsync.CallBackTask(){
			public void callBack(boolean result)
			{
				initDisplayItem();
			};
		});
		http.execute(urls);
		return true;
	}
	
	// 退勤
	private boolean endOfWork(String userid, String password)
	{
		String urls[] = {URL_LOGIN, URL_LEAVE, userid, password};
		StampUseHttpAsync http = new StampUseHttpAsync(this);
		http.setOnCallBack(new StampUseHttpAsync.CallBackTask(){
			public void callBack(boolean result)
			{
				initDisplayItem();
			};
		});
		http.execute(urls);
		return true;
	}
	
	// 今日スタンプされたか返す
	private boolean isStampedToday()
	{
		SaveSetting setting = new SaveSetting(MainActivity.this);
		String stamp = setting.getLastStamp();

		String nowdate = (String) DateFormat.format("yyyy/MM/dd", Calendar.getInstance());
		if (nowdate.compareTo(stamp) == 0)
		{
			return true;
		}
		return false;
	}
	
	/** タグ登録済みか */
	private boolean isRegisteredTag()
	{
		SaveSetting setting = new SaveSetting(MainActivity.this);
		String tag = setting.getIdm();

		if (tag.isEmpty())
		{
			return false;
		}
		return true;
	}
}
