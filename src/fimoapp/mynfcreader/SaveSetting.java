package fimoapp.mynfcreader;


import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SaveSetting extends SQLiteOpenHelper {
	/** テーブル名 */
	private String TBL_SAVE = "SAVE";
	private String KEY_LASTSTAMP = "LASTSTAMP";
	private String KEY_IDM = "IDM";
	private String KEY_USERID = "USERID";
	private String KEY_PASSWORD = "PASSWORD";
	
	/** データ */
	private SaveSettingData data_ = null;
	
	/** コンストラクタ */
	public SaveSetting(Context context) {
		super(context, context.getString(R.string.app_name) + ".db", null, 1);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		createTable();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	
	/** テーブル作成 */
	private void createTable()
	{
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor c = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='"+TBL_SAVE+"';", null);
			c.moveToFirst();
			int raw = c.getInt(0);
			c.close();
			if (raw == 0)
			{
				long errorcode;
				// テーブルがない
				db.execSQL("create table "+ TBL_SAVE +"( KEY TEXT, VALUE1 TEXT, VALUE2 TEXT );");
				// 値ペアの設定
				// idm
				ContentValues values = new ContentValues();
				values.put("KEY", KEY_IDM);
				values.put("VALUE1", "");
				values.put("VALUE2", "");
				errorcode = db.insert(TBL_SAVE, null, values);
				if (errorcode < 0){
					db.close();
					return;
				}
				
				// userid
				values = new ContentValues();
				values.put("KEY", KEY_USERID);
				values.put("VALUE1", "");
				values.put("VALUE2", "");
				errorcode = db.insert(TBL_SAVE, null, values);
				if (errorcode < 0){
					db.close();
					return;
				}
				
				// password
				values = new ContentValues();
				values.put("KEY", KEY_PASSWORD);
				values.put("VALUE1", "");
				values.put("VALUE2", "");
				errorcode = db.insert(TBL_SAVE, null, values);
				if (errorcode < 0){
					db.close();
					return;
				}
				
				// laststamp
				values = new ContentValues();
				values.put("KEY", KEY_LASTSTAMP);
				values.put("VALUE1", "");
				values.put("VALUE2", "");
				errorcode = db.insert(TBL_SAVE, null, values);
				if (errorcode < 0){
					db.close();
					return;
				}
			}
			db.close();
		} catch(Exception e) {
			System.out.println(e.toString());
		}
	}
	
	/** IDMをget */
	public String getIdm()
	{
		if (getData() == false) return "";
		
		return data_.getIdm();
	}
	
	/** LastStampをget */
	public String getLastStamp()
	{
		if (getData() == false) return "";
		
		return data_.getLaststamp();
	}
	
	/** useridをget */
	public String getUserid()
	{
		if (getData() == false) return "";
		
		return data_.getUserid();
	}
	
	/** passwordをget */
	public String getPassword()
	{
		if (getData() == false) return "";
		
		return data_.getPassword();
	}
	
	/** lastを更新 */
	public boolean setLastStamp(String stamp)
	{
		if (getData() == false) return false;
		
		data_.setLaststamp(stamp);
		
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			
			// 値ペアの設定
			ContentValues values = new ContentValues();
			values.put("KEY", KEY_LASTSTAMP);
			values.put("VALUE1", stamp);
			values.put("VALUE2", "");
			int updrows = db.update(TBL_SAVE, values, "KEY = ?", 
					new String[]{
					KEY_LASTSTAMP});
		
			if (updrows < 1){
				db.close();
				return false;
			}
			
			db.close();
		} catch(Exception e) {
			System.out.println(e.toString());
		}
		return true;
	}
	
	/** ユーザー情報登録 */
	public boolean setUserInfo(String userid, String password)
	{
		if (getData() == false) return false;
		data_.setUserid(userid);
		data_.setPassword(password);
		
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			
			// 値ペアの設定
			ContentValues values = new ContentValues();
			values.put("KEY", KEY_USERID);
			values.put("VALUE1", userid);
			values.put("VALUE2", "");
			int updrows = db.update(TBL_SAVE, values, "KEY = ?", 
					new String[]{
					KEY_USERID});
		
			if (updrows < 1){
				db.close();
				return false;
			}
			// 値ペアの設定
			values = new ContentValues();
			values.put("KEY", KEY_PASSWORD);
			values.put("VALUE1", password);
			values.put("VALUE2", "");
			updrows = db.update(TBL_SAVE, values, "KEY = ?", 
					new String[]{
					KEY_PASSWORD});
		
			if (updrows < 1){
				db.close();
				return false;
			}

			db.close();
		} catch(Exception e) {
			System.out.println(e.toString());
		}
		return true;
	}
	
	/** idm設定 */
	public boolean setIdm(String idm)
	{
		if (getData() == false) return false;
		
		data_.setIdm(idm);
		
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			
			// 値ペアの設定
			ContentValues values = new ContentValues();
			values.put("KEY", KEY_IDM);
			values.put("VALUE1", idm);
			values.put("VALUE2", "");
			int updrows = db.update(TBL_SAVE, values, "KEY = ?", 
					new String[]{
					KEY_IDM});
		
			if (updrows < 1){
				db.close();
				return false;
			}
			
			db.close();
		} catch(Exception e) {
			System.out.println(e.toString());
		}
		return true;
	}
		
	/** dbから取得 */
	private boolean getData()
	{
		if (data_ != null) return true;
		
		data_ = new SaveSettingData();
		
		createTable();
		SQLiteDatabase db = this.getWritableDatabase();
		
		try {
			// cursor
			Cursor c = db.query(TBL_SAVE, 
					new String[]{"KEY", "VALUE1", "VALUE2"}
			, null, null, null, null, "KEY asc");
			
			String idm = new String("");
			String laststamp = new String("");
			String userid = new String("");
			String password = new String("");
			
			boolean is_eof = c.moveToFirst();
			while(is_eof) {
				String key = c.getString(0);
				String value = c.getString(1);
				
				if (key.compareTo(KEY_IDM) == 0)
				{
					idm = value;
				}
				else if (key.compareTo(KEY_USERID) == 0)
				{
					userid = value;
				}
				else if (key.compareTo(KEY_PASSWORD) == 0)
				{
					password = value;
				}
				else if (key.compareTo(KEY_LASTSTAMP) == 0)
				{
					laststamp = value;
				}
				is_eof = c.moveToNext();
			}
			c.close();
			
			data_.setData(idm, userid, password, laststamp);
			
		} catch(Exception e) {
			System.out.println(e.toString());
			db.close();
			return false;
		}
		db.close();
		return true;
	}

}
