package fimoapp.mynfcreader;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PersonListAdapter extends ArrayAdapter<PersonListItem> {
	
	private LayoutInflater inflater;
	private int textViewResourceId;
	private List<PersonListItem> items;

	public PersonListAdapter(Context context,
			int textViewResourceId, List<PersonListItem> objects) {
		super(context, textViewResourceId, objects);
		
		// 退避
		this.textViewResourceId = textViewResourceId;
		this.items = objects;
		// インフレーター
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		//return super.getView(position, convertView, parent);
		View view;
		if (convertView != null) {
			// 再利用
			view = convertView;
		} else {
			// 新規作成
			view = inflater.inflate(textViewResourceId, null);
		}
		
		// 対象のアイテム取得
		PersonListItem item = items.get(position);
		
		// 名前
		TextView text_name = (TextView) view.findViewById(R.id.personlistitem_name);
		text_name.setText(item.getName());
		
		// でばぐよう
		((TextView)view.findViewById(R.id.personlistitem_idm)).setText(item.getIdm());
		//((TextView)view.findViewById(R.id.personlistitem_userid)).setText(item.getUserid());
		//((TextView)view.findViewById(R.id.personlistitem_password)).setText(item.getPassword());
		//((TextView)view.findViewById(R.id.personlistitem_laststamp)).setText(item.getLaststamp());
		
		return view;
	}

}
