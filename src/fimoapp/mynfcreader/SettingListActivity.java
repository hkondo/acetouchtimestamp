package fimoapp.mynfcreader;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.view.View;

/**
 * 設定画面
 * @author hitomi
 *
 */
public class SettingListActivity extends Activity {

	private static final int Pos_UserInfo = 0;
	private static final int Pos_GetTag = 1;
	private static final int Pos_AllClear = 2;
	private static final String Str_UserInfo = "ユーザー情報設定";
	private static final String Str_GetTag = "タグIDの取得";
	private static final String Str_AllClear = "設定をクリア";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// 画面レイアウトの設定
		Intent intent = getIntent();
		setContentView(R.layout.settinglistview);
		
		// 設定リスト
		String[] members = { Str_UserInfo, Str_GetTag, Str_AllClear};
		ListView lv = (ListView) findViewById(R.id.setting_list);
 
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        		android.R.layout.simple_list_item_1, members);
        lv.setAdapter(adapter);
        
        // リストビューのアイテムがクリックされた時に呼び出されるコールバックリスナーを登録します
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	@Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                ListView listView = (ListView) parent;
                if (position == Pos_UserInfo)
                {
                	// ユーザー情報
                	//Intent intent2 = new Intent(SettingListActivity.this, LoginFormActivity.class);
            		//startActivity(intent2);
                	FragmentManager manager = getFragmentManager();
    				Bundle args = new Bundle();

    				LoginFormDialg dialog = new LoginFormDialg();
    				// 
    				dialog.show(manager, "dialog");
                }
                else if (position == Pos_GetTag)
                {
                	// タグ取得
                	//String urls[] = {URL_LOGIN, URL_LEAVE, userid, password};
                	GetTagHttpAsync http = new GetTagHttpAsync(SettingListActivity.this);
            		http.execute();
                }
                else if (position == Pos_AllClear)
                {
                	// クリア
                	SaveSetting setting = new SaveSetting(SettingListActivity.this);
                	setting.setIdm("");
                	setting.setLastStamp("");
                	setting.setUserInfo("", "");
                	Toast.makeText(SettingListActivity.this, "設定をクリアしました。", Toast.LENGTH_LONG).show();
                }
            }
        });
	}
	
    @Override
	protected void onResume() {
		super.onResume();
    }
}
